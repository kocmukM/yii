<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TableAsset extends AssetBundle
{
    public $sourcePath = '@app/themes/table';


    public $css = [
        'css/bootstrap.css',
        'css/font-awesome.min.css',
        'css/linearicons.css',
        'css/magnific-popup.css',
        'css/main.css',
        'css/nice-select.css',
        'css/owl.carousel.css',

    ];
    public $js = [
        'js/jquery.ajaxchimp.min.js',
        'js/jquery.min.j',
        'js\table\jquery.magnific-popup.min.js',
        'js/jquery.nice-select.min.js',
        'js/main.js',
        'js/owl.carousel.min.js',
        'js/pgwslider.min.js',
        'js/waypoints.min.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset'
    ];
}