<?php

use yii\db\Migration;
use yii\db\mysql\Schema;


/**
 * Class m190213_065123_version1
 */
class m190213_065123_version1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190213_065123_version1 cannot be reverted.\n";

        return false;
    }

    public function up()
    {
        $this->createTable('magazine', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'published_at' => Schema::TYPE_DATE.' NOT NULL',
            'votes' => Schema::TYPE_INTEGER.' NOT NULL',
            'is_favorite' => Schema::TYPE_BOOLEAN.' NOT NULL',
            'review' => Schema::TYPE_TEXT,
            'publisher_name' => Schema::TYPE_STRING.' NOT NULL'
        ]);

        $this->createTable('post', [
            'id' => Schema::TYPE_PK,
            'magazine_id' => Schema::TYPE_INTEGER.' NOT NULL',
            'content' => Schema::TYPE_TEXT.' NOT NULL'
        ]);

        $this->addForeignKey('belong', 'post', 'magazine_id', 'magazine', 'id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190213_065123_version1 cannot be reverted.\n";

        return false;
    }
    */
}
