<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "magazine".
 *
 * @property int $id
 * @property string $title
 * @property string $published_at
 * @property int $votes
 * @property int $is_favorite
 * @property string $review
 * @property string $publisher_name
 *
 * @property Post[] $posts
 */
class Magazine extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'magazine';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'published_at', 'votes', 'is_favorite', 'publisher_name'], 'required'],
            [['published_at'], 'safe'],
            [['votes', 'is_favorite'], 'integer'],
            [['review'], 'string'],
            [['title', 'publisher_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'published_at' => 'Published At',
            'votes' => 'Votes',
            'is_favorite' => 'Is Favorite',
            'review' => 'Review',
            'publisher_name' => 'Publisher Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['magazine_id' => 'id']);
    }
}
