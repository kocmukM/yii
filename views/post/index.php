<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$customTitle = 'Posts table';
?>
<div class="post-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <center>
        <h2 class="text-white">Working with <span><?= $customTitle ?></span></h2>
    </center>
    <p>
    <center>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </center>
    </p>

    <div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'magazine_id',
                'content:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
