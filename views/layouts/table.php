<?php
/* @var $content string */

use app\assets\TableAsset;
use yii\helpers\Html;

TableAsset::register($this);
?>

<?php $this->beginPage() ?>
    <html lang="en">
    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon-->
        <link rel="shortcut icon" href="img/fav.png">
        <!-- Author Meta -->
        <meta name="author" content="Colorlib">
        <!-- Meta Description -->
        <meta name="description" content="">
        <!-- Meta Keyword -->
        <meta name="keywords" content="">
        <!-- meta character set -->
        <meta charset="UTF-8">
        <!-- Site Title -->
        <title>CellOn</title>
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
        <?php $this->head() ?>

    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="oz-body-wrap">
        <!-- Start Header Area -->
        <header class="default-header generic-header">
            <div class="container-fluid">
                <div class="header-wrap">
                    <div class="header-top d-flex justify-content-between align-items-center">
                        <button>
                            <?= HTML::a('To home', 'site/') ?>
                        </button>
                        <button>
                            <?= HTML::a('To magazine table', '?r=magazine/') ?>
                        </button>
                        <button>
                            <?= HTML::a('To post table', '?r=post/') ?>
                        </button>
                        <div class="logo">
                            <a href="index.html"><img src="img/logo.png" alt=""></a>
                        </div>
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide">
                                <a href="index.html">Home</a>
                                <a href="generic.html">Generic</a>
                                <a href="elements.html">Elements</a>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End Header Area -->
        <!-- Start Banner Area -->
        <section class="generic-banner elements-banner relative">
            <div class="container">
                <div class="row height align-items-center justify-content-center">
                    <div class="col-lg-10">
                        <div class="banner-content text-center">
                            <h2>The Elements Page</h2>
                            <p>It won’t be a bigger problem to find one video game lover in your <br> neighbor. Since
                                the introduction of Virtual Game.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Banner Area -->


        <!-- Start Conatct- Area -->
        <section class="contact-area pt-100 pb-100 relative cactus-container"
                 style="background: rgba(108, 187, 35, 0.85)">

            <div style="margin-left: 100px; margin-right: 100px">
                <?= $content ?>
            </div>

        </section>
        <!-- End Conatct- Area -->
        <!-- Strat Footer Area -->
        <footer class="section-gap">
            <div class="container">
                <div class="row pt-60">
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="text-uppercase mb-20">Top Product</h6>
                            <ul class="footer-nav">
                                <li><a href="#">Managed Website</a></li>
                                <li><a href="#">Manage Reputation</a></li>
                                <li><a href="#">Power Tools</a></li>
                                <li><a href="#">Marketing Service</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="text-uppercase mb-20">Navigation</h6>
                            <ul class="footer-nav">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Main Features</a></li>
                                <li><a href="#">Offered Services</a></li>
                                <li><a href="#">Latest Portfolio</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="text-uppercase mb-20">Compare</h6>
                            <ul class="footer-nav">
                                <li><a href="#">Works & Builders</a></li>
                                <li><a href="#">Works & Wordpress</a></li>
                                <li><a href="#">Works & Templates</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="text-uppercase mb-20">Quick About</h6>
                            <p>
                                Lorem ipsum dolor sit amet, consecteturadipisicin gelit, sed do eiusmod tempor
                                incididunt.
                            </p>
                            <p>
                                +00 012 6325 98 6542 <br>
                                support@colorlib.com
                            </p>
                            <div class="footer-social d-flex align-items-center">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    <p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                        All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                            aria-hidden="true"></i> by <a
                                href="https://colorlib.com" target="_blank">Colorlib</a></p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>