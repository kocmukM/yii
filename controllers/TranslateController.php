<?php

namespace app\controllers;

use yii\web\Controller;


class TranslateController extends Controller
{

    public function actionSpeedConvert($speedmps = 0)
    {
        return $this->render('translate', ['result' =>'~'.round($speedmps / 3.6, 2).' km per hour']);
    }
}
