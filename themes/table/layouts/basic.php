<?use app\assets\TableAsset;
use yii\helpers\Html;

TableAsset::register($this);
$bundle = $this->getAssetManager()->getBundle('app\assets\TableAsset');
$images = Yii::getAlias('@web').'/images'
?>

<?php $this->beginPage() ?>
<html lang="en">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="Colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>CellOn</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
<div class="oz-body-wrap">
    <!-- Start Header Area -->
    <header class="default-header generic-header" >
        <div class="container-fluid">
            <div class="header-wrap">
                <div class="header-top d-flex justify-content-between align-items-center">
                    <div class="logo">
                        <a href="index.html"><img src="img/logo.png" alt=""></a>
                    </div>
                    <div class="main-menubar d-flex align-items-center">
                        <nav>
                            <?= HTML::a('Home', 'site/') ?>
                            <?= HTML::a('Magazine table', '?r=magazine/') ?>
                            <?= HTML::a('Post table', '?r=post/') ?>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- End Header Area -->
    <!-- Start Banner Area -->
    <section class="generic-banner elements-banner relative" style="background-image: url('<?=$images?>/cactus.jpeg'); background-size: cover;  ">
        <div class="container">
            <div class="row height align-items-center justify-content-center">
                <div class="col-lg-10">
                    <div class="banner-content text-center">
                        <h2>The Elements Page</h2>
                        <p>It won’t be a bigger problem to find one video game lover in your <br> neighbor. Since
                            the introduction of Virtual Game.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Banner Area -->
    <!-- Start faq Area -->
    <section class="faq-area pt-100 pb-100">
        <div class="container">
            <div class="row">
                <div class="counter-left col-lg-3 col-md-3">
                    <div class="single-facts">
                        <h2 class="counter">5962</h2>
                        <p>Projects Completed</p>
                    </div>
                    <div class="single-facts">
                        <h2 class="counter">2394</h2>
                        <p>New Projects</p>
                    </div>
                    <div class="single-facts">
                        <h2 class="counter">1439</h2>
                        <p>Tickets Submitted</p>
                    </div>
                    <div class="single-facts">
                        <h2 class="counter">933</h2>
                        <p>Cup of Coffee</p>
                    </div>
                </div>
                <div class="faq-content col-lg-9 col-md-9">
                    <div class="single-faq">
                        <h2 class="text-uppercase">
                            Are your Templates responsive?
                        </h2>
                        <p>
                            “Few would argue that, despite the advancements of feminism over the past three decades, women still face a double standard when it comes to their behavior. While men’s borderline-inappropriate behavior.
                        </p>
                    </div>
                    <div class="single-faq">
                        <h2 class="text-uppercase">
                            Does it have all the plugin as mentioned?
                        </h2>
                        <p>
                            “Few would argue that, despite the advancements of feminism over the past three decades, women still face a double standard when it comes to their behavior. While men’s borderline-inappropriate behavior.
                        </p>
                    </div>
                    <div class="single-faq">
                        <h2 class="text-uppercase">
                            Can i use the these theme for my client?
                        </h2>
                        <p>
                            “Few would argue that, despite the advancements of feminism over the past three decades, women still face a double standard when it comes to their behavior. While men’s borderline-inappropriate behavior.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div style="margin-left: 100px; margin-right: 100px">
        <?= $content ?>
    </div>
    <!-- Start Conatct- Area -->

    <section class="contact-area pt-100 pb-100 relative">
        <div class="overlay overlay-bg"></div>
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="single-contact col-lg-6 col-md-8">
                    <h2 class="text-white">Send Us <span>Message</span></h2>
                    <p class="text-white">
                        Most people who work in an office environment, buy computer products.
                    </p>
                </div>
            </div>
            <form id="myForm" action="mail.php" method="post" class="contact-form">
                <div class="row justify-content-center">
                    <div class="col-lg-5">
                        <input name="fname" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" class="common-input mt-20" required="" type="text">
                    </div>
                    <div class="col-lg-5">
                        <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" class="common-input mt-20" required="" type="email">
                    </div>
                    <div class="col-lg-10">
                        <textarea class="common-textarea mt-20" name="message" placeholder="Messege" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                    </div>
                    <div class="col-lg-10 d-flex justify-content-end">
                        <button class="primary-btn white-bg d-inline-flex align-items-center mt-20"><span class="mr-10">Send Message</span><span class="lnr lnr-arrow-right"></span></button> <br>
                    </div>
                    <div class="alert-msg"></div>
                </div>
            </form>
        </div>
    </section>
    <!-- End Conatct- Area -->
    <!-- Strat Footer Area -->
    <footer class="section-gap">
        <div class="container">
            <div class="row pt-60">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6 class="text-uppercase mb-20">Top Product</h6>
                        <ul class="footer-nav">
                            <li><a href="#">Managed Website</a></li>
                            <li><a href="#">Manage Reputation</a></li>
                            <li><a href="#">Power Tools</a></li>
                            <li><a href="#">Marketing Service</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6 class="text-uppercase mb-20">Navigation</h6>
                        <ul class="footer-nav">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Main Features</a></li>
                            <li><a href="#">Offered Services</a></li>
                            <li><a href="#">Latest Portfolio</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6 class="text-uppercase mb-20">Compare</h6>
                        <ul class="footer-nav">
                            <li><a href="#">Works & Builders</a></li>
                            <li><a href="#">Works & Wordpress</a></li>
                            <li><a href="#">Works & Templates</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h6 class="text-uppercase mb-20">Quick About</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consecteturadipisicin gelit, sed do eiusmod tempor
                            incididunt.
                        </p>
                        <p>
                            +00 012 6325 98 6542 <br>
                            support@colorlib.com
                        </p>
                        <div class="footer-social d-flex align-items-center">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-dribbble"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom d-flex justify-content-between align-items-center flex-wrap">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                <p class="footer-text m-0">Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                    All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                        aria-hidden="true"></i> by <a
                            href="https://colorlib.com" target="_blank">Colorlib</a></p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>